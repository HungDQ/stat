<?php

class Magebuzz_Numberofview_Helper_Data extends Mage_Core_Helper_Data
{
  public function getCurrentUrl()
  {
    $url = Mage::getUrl('', array(
      '_current' => true,
      '_use_rewrite' => true
    ));

    return $url;
  }
}
