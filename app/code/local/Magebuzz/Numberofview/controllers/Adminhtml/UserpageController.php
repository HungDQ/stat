<?php

class Magebuzz_Numberofview_Adminhtml_UserpageController extends Mage_Adminhtml_Controller_Action
{
  public function indexAction()
  {
    $this->loadLayout();
    $this->_addContent($this->getLayout()->createBlock('numberofview/adminhtml_UserPage'));
    $this->renderLayout();
  }

  public function exportCsvAction()
  {
    $fileName = 'Sales_Report.csv';
    $content = $this->getLayout()->createBlock('numberofview/adminhtml_customer_edit_tab_stat')->getCsv();
    $this->_sendUploadResponse($fileName, $content);
  }

  protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream')
  {
    $response = $this->getResponse();
    $response->setHeader('HTTP/1.1 200 OK', '');
    $response->setHeader('Pragma', 'public', TRUE);
    $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', TRUE);
    $response->setHeader('Content-Disposition', 'attachment; filename=' . $fileName);
    $response->setHeader('Last-Modified', date('r'));
    $response->setHeader('Accept-Ranges', 'bytes');
    $response->setHeader('Content-Length', strlen($content));
    $response->setHeader('Content-type', $contentType);
    $response->setBody($content);
    $response->sendResponse();
    die;
  }

  public function vcustomerAction()
  {
    $this->loadLayout();
    $this->getLayout()->getBlock('customer.edit.tab.stat')
      ->setStat($this->getRequest()->getPost('ovostatendor', null));
    $this->renderLayout();
  }

  public function vcustomerGridAction()
  {
    $this->loadLayout();
    $this->getLayout()->getBlock('customer.edit.tab.stat')
      ->setStat($this->getRequest()->getPost('ostat', null));
    $this->renderLayout();
  }
}

