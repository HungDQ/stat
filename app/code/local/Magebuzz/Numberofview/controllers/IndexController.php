<?php
class Magebuzz_Numberofview_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $configValue = Mage::getStoreConfig('numberofview/numberofview_group');
        $array_group_id = '';
        $config_page_sales = 0;
        $config_page_stats = 0;
        $customer_group_id = 0;
        try {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $customer_group_id = $customer['group_id'];
        } catch (Exception $e) {

        }
        // print_r($configValue);
        // die;
        if (isset($configValue['config_group'])) {
            $array_group_id = explode(',',$configValue['config_group']);
            if (count($array_group_id) > 1) {
                foreach ($array_group_id as $value) {
                    if ($value == $customer_group_id) {
                        if(Isset($_GET['total_sales'])){
                            if ($_GET['total_sales'] != "" ) {
                                if (isset($configValue['config_page_sales'])) {
                                    if ($configValue['config_page_sales'] == 1) {
                                        $this->_title($this->__('Customer Sales Stats'));
                                        $this->loadLayout();
                                        $this->renderLayout();
                                    }else{
                                        $base_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
                                        $this->_redirect('customer/account/');
                                    }
                                }
                            }
                        }
                        else if(Isset($_GET['page_stats'])){
                            if ($_GET['page_stats'] != "" ) {
                                if (isset($configValue['config_page_stats'])) {
                                    if ($configValue['config_page_stats'] == 1) {
                                        $this->_title($this->__('Customer Page View Stats'));
                                        $this->loadLayout();
                                        $this->renderLayout();
                                    }else{
                                        $base_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
                                        $this->_redirect('customer/account/');
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                foreach ($array_group_id as $value){
                    if ($value == $customer_group_id) {
                        if(Isset($_GET['total_sales'])){
                            if ($_GET['total_sales'] != "" ) {
                                if (isset($configValue['config_page_sales'])) {
                                    if ($configValue['config_page_sales'] == 1) {
                                        $this->_title($this->__('Customer Sales Stats'));
                                        $this->loadLayout();
                                        $this->renderLayout();
                                    }else{
                                        $base_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
                                        $this->_redirect('customer/account/');
                                    }
                                }
                            }
                        }
                        else if(Isset($_GET['page_stats'])){
                            if ($_GET['page_stats'] != "" ) {
                                if (isset($configValue['config_page_stats'])) {
                                    if ($configValue['config_page_stats'] == 1) {
                                        $this->_title($this->__('Customer Page View Stats'));
                                        $this->loadLayout();
                                        $this->renderLayout();
                                    }else{
                                        $base_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
                                        $this->_redirect('customer/account/');
                                    }
                                }
                            }
                        }
                    }else{
                        $base_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
                        $this->_redirect('customer/account/');
                    }
                }
            }
        }else{
            $base_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
            $this->_redirect('customer/account/');
        }
    }
}
