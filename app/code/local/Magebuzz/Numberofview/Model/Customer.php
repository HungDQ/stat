<?php

class Magebuzz_Numberofview_Model_Customer extends Mage_Core_Model_Abstract
{
  public function _construct()
  {
    parent::_construct();
    $this->_init('numberofview/customer');
  }

  public function saveCustomerStat($pageType)
  {
    $url = Mage::helper('numberofview')->getCurrentUrl();
    $curCustomerId = Mage::getSingleton('customer/session')->getCustomerId();
    if(!$curCustomerId){
      $curCustomerId = 0;
    }
    $oldPageId = $this->getCollection()->addFieldToFilter('url', $url)->addFieldToFilter('customer_id', $curCustomerId)->getFirstItem()->getId();
    if($oldPageId){
      $this->load($oldPageId);
      $pageView = $this->getPageView() + 1;
      $this->setPageView($pageView);
      $this->save();
    }else{
      $data = array(
        'customer_id'=>$curCustomerId,
        'url'=>$url,
        'page_type'=>$pageType,
        'time'=>date("Y-m-d H:i:s"),
      );
      $this->setData($data);
      try {
        $this->save();
      } catch (Exception $e){
      }
    }
  }
}
