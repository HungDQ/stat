<?php

class Magebuzz_Numberofview_Model_Numberofview extends Mage_Core_Model_Abstract
{
  public function _construct()
  {
    parent::_construct();
    $this->_init('numberofview/numberofview');
  }

  public function savePageStat($pageType, $pageDetail, $oldPageId)
  {
    $url = Mage::helper('numberofview')->getCurrentUrl();
    if ($oldPageId) {
      $this->load($oldPageId);
      $pageView = $this->getPageView() + 1;
      $this->setPageView($pageView);
      try {
        $this->save();
      } catch (Exception $e) {
      }
    } else {
      $data = array(
        'page_type' => $pageType,
        'page_detail' => $pageDetail,
        'url' => $url,
        'time' => date("Y-m-d H:i:s"),
      );
      $this->setData($data);
      try {
        $this->save();
      } catch (Exception $e) {
      }
    }
  }
}
