<?php

class Magebuzz_Numberofview_Model_Observer
{
  public function beforeLoadLayout($observer)
  {
    $pageType = '';
    $pageDetail = '';
    $routeName = Mage::app()->getFrontController()->getRequest()->getRouteName();
    $controllerName = Mage::app()->getFrontController()->getRequest()->getControllerName();
    $url = Mage::helper('numberofview')->getCurrentUrl();

    if($routeName == 'cms'){
      $pageDetail = Mage::getSingleton('cms/page')->getIdentifier();
      $pageType = $routeName;
    }
    if ($routeName == 'catalog') {
      if ($controllerName == 'category') {
        $pageDetail = Mage::registry('current_category')->getName();
      }
      if ($controllerName == 'product') {
        $pageDetail = Mage::registry('current_product')->getName();
      }
      $pageType = $controllerName;
    }
    if (($pageDetail != 'no-route') && ($routeName == 'cms' || $routeName == 'catalog')){
      $model = Mage::getModel('numberofview/numberofview');
      $oldPageId = $model->getCollection()->addFieldToFilter('url', $url)->getFirstItem()->getId();
      $model->savePageStat($pageType, $pageDetail, $oldPageId);
      $customerStatModel = Mage::getModel('numberofview/customer');
      $customerStatModel->saveCustomerStat($pageType);
    }
  }

  public function addStatTabToCustomer($observer)
  {
    $block = $observer->getEvent()->getBlock();
    $request = Mage::app()->getRequest();

    if ($block instanceof Mage_Adminhtml_Block_Customer_Edit_Tabs) {
      if ($request->getActionName() == 'edit') {
        $block->addTab('customer_stat', array(
          'label' => Mage::helper('numberofview')->__('Statistic'),
          'title' => Mage::helper('numberofview')->__('Statistic'),
          'url' => $block->getUrl('numberofview/adminhtml_userpage/vcustomer', array('_current' => true)),
          'class' => 'ajax',
        ));
      }
    }
  }
}
