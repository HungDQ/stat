<?php
$installer = $this;
$installer->startSetup();

$installer->run("
  ALTER TABLE `{$this->getTable('numberofview/numberofview')}` ADD COLUMN `page_view` int(11) NOT NULL DEFAULT 1;
");
$installer->endSetup();