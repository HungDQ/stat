<?php
$installer = $this;
$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('numberofview/customer')};
CREATE TABLE {$this->getTable('numberofview/customer')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `customer_id` int(11) unsigned NOT NULL,
  `url` text NOT NULL,
  `page_type` varchar(255) NOT NULL,
  `page_view` int(11) NOT NULL default 1,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 