<?php
class Magebuzz_Numberofview_Block_Account_Navigation extends Mage_Customer_Block_Account_Navigation
{
    public function removeLinkByName($name) {
        unset($this->_links[$name]);
    }
    public function addLink($name, $path, $label, $urlParams=array())
    {
        $configValue = Mage::getStoreConfig('numberofview/numberofview_group');
        $array_group_id = '';
        $config_page_sales = 0;
        $config_page_stats = 0;
        $customer_group_id = 0;
        try {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $customer_group_id = $customer['group_id'];
        } catch (Exception $e) {

        }
        if ($name != 'page_stats' && $name != 'total_sales') {
            $this->_links[$name] = new Varien_Object(array(
                'name' => $name,
                'path' => $path,
                'label' => $label,
                'url' => $this->getUrl($path, $urlParams),
            ));
        }else{
            if (isset($configValue['config_group'])) {
                $array_group_id = explode(',',$configValue['config_group']);
                foreach ($array_group_id as $value) {
                    if ($value == $customer_group_id) {

                        if ($name == "total_sales") {
                            if (isset($configValue['config_page_sales'])) {
                                if ($configValue['config_page_sales'] == 1) {
                                    $this->_links[$name] = new Varien_Object(array(
                                        'name' => $name,
                                        'path' => $path,
                                        'label' => $label,
                                        'url' => $this->getUrl($path, $urlParams),
                                    ));
                                }
                            }
                        }
                        if ($name == "page_stats") {
                            if (isset($configValue['config_page_stats'])) {
                                if ($configValue['config_page_stats'] == 1) {
                                    $this->_links[$name] = new Varien_Object(array(
                                        'name' => $name,
                                        'path' => $path,
                                        'label' => $label,
                                        'url' => $this->getUrl($path, $urlParams),
                                    ));
                                }
                            }
                        }
                    }
                }
            }
        }
        return $this;
    }
}
