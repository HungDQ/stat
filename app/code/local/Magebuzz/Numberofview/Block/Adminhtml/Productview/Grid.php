<?php
class Magebuzz_Numberofview_Block_Adminhtml_Productview_Grid extends Mage_Adminhtml_Block_Widget_Grid{
  public function __construct()
  {
    parent::__construct();
    $this->setId('adminhtml_productview');
    $this->setDefaultSort('id');
    $this->setDefaultDir('ASC');
    $this->setSaveParametersInSession(TRUE);
  }

  protected function _prepareCollection()
  {
    $collection = Mage::getResourceModel('customer/customer_collection')
      ->addNameToSelect()
      ->addAttributeToSelect('email')
      ->addAttributeToSelect('created_at')
      ->addAttributeToSelect('group_id')
      ->joinAttribute('company', 'customer_address/company', 'default_billing', null, 'left');

    $statTable = Mage::getSingleton('core/resource')->getTableName('stat_customer');
    $collection->getSelect()
      ->join(array("stat" => $statTable), "e.entity_id = stat.customer_id", array("*","sum(stat.page_view) as page_view" ))
      ->where('stat.page_type = "product"')
      ->group('e.entity_id');

    $this->setCollection($collection);
    return parent::_prepareCollection();
  }

  protected function _setCollectionOrder($column)
  {
    $collection = $this->getCollection();
    if ($collection) {
      $columnIndex = $column->getFilterIndex() ? $column->getFilterIndex() : $column->getIndex();
      if($columnIndex == 'page_view')
      {
        $collection->getSelect()->order($columnIndex.' '.strtoupper($column->getDir()));
      }
      else{
        $collection->setOrder($columnIndex, strtoupper($column->getDir()));
      }
    }
    return $this;
  }

  protected function _prepareColumns()
  {
    $this->addExportType('*/*/exportCsv',
      Mage::helper('numberofview')->__('CSV'));

    $this->addColumn('email', array(
      'header' => Mage::helper('numberofview')->__('Customer Email'),
      'index' => 'email',
    ));

    $this->addColumn('company', array(
      'header' => Mage::helper('numberofview')->__('Company'),
      'index' => 'company',
    ));

    $this->addColumn('firstname', array(
      'header' => Mage::helper('numberofview')->__('First Name'),
      'index' => 'firstname',
    ));

    $this->addColumn('lastname', array(
      'header' => Mage::helper('numberofview')->__('Last Name'),
      'index' => 'lastname',
    ));

    $groups = Mage::getResourceModel('customer/group_collection')
      ->addFieldToFilter('customer_group_id', array('gt' => 0))
      ->load()
      ->toOptionHash();

    $this->addColumn('group', array(
      'header' => Mage::helper('customer')->__('Group'),
      'width' => '100',
      'index' => 'group_id',
      'type' => 'options',
      'options' => $groups,
    ));

    $this->addColumn('customer_since', array(
      'header' => Mage::helper('customer')->__('Customer Since'),
      'type' => 'datetime',
      'align' => 'center',
      'index' => 'created_at',
      'gmtoffset' => true
    ));

    $this->addColumn('page_view', array(
      'header' => Mage::helper('numberofview')->__('Product Page View'),
      'index' => 'page_view',
    ));

    $this->addColumn('action', array(
      'header' => Mage::helper('numberofview')->__('Action'),
      'index' => 'entity_id',
      'filter' => false,
      'sortable' => false,
      'renderer' => 'Magebuzz_Numberofview_Block_Adminhtml_Productview_Renderer',
      'is_system'   => true
    ));
    return parent::_prepareColumns();
  }
}
