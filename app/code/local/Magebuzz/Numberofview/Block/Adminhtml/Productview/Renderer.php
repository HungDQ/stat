<?php
class Magebuzz_Numberofview_Block_Adminhtml_Productview_Renderer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    public function render(Varien_Object $row) {
      $value = $row->getData($this->getColumn()->getIndex());
      $url = $this->getUrl("adminhtml/customer/edit/", array("id"=>$value, 'active_tab' => 'customer_stat')).'?';
      return '<a href="'.$url.'">View</a>';
    }
}
