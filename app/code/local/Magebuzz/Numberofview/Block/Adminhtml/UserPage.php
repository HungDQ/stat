<?php
class Magebuzz_Numberofview_Block_Adminhtml_UserPage extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'numberofview';
        $this->_controller = 'adminhtml_UserPage';
        $this->_addButtonLabel = Mage::helper('numberofview')->__('Add Item');
        $this->_headerText = Mage::helper('numberofview')->__('');
        parent::__construct();
        $this->_removeButton('add');
    }

}
