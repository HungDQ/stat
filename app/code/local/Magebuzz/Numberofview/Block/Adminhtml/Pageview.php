<?php
class Magebuzz_Numberofview_Block_Adminhtml_Pageview extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_pageview';
        $this->_blockGroup = 'numberofview';
        $this->_headerText = Mage::helper('numberofview')->__('Report page');
        parent::__construct();
        $this->_removeButton('add');
    }
}
