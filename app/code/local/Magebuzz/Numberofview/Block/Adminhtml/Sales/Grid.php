<?php

class Magebuzz_Numberofview_Block_Adminhtml_Sales_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
    parent::__construct();
    $this->setId('adminhtml_sales');
    $this->setDefaultSort('id');
    $this->setSaveParametersInSession(TRUE);
  }

  protected function _prepareCollection()
  {
    $collection = Mage::getResourceModel('customer/customer_collection')
      ->addNameToSelect()
      ->addAttributeToSelect('email')
      ->addAttributeToSelect('created_at')
      ->addAttributeToSelect('group_id');
    $collection->addSalesInfoPerCustomer();
    $this->setCollection($collection);
    parent::_prepareCollection();
    return $this;
  }

  protected function _setCollectionOrder($column)
  {
    $collection = $this->getCollection();
    if ($collection) {
      $columnIndex = $column->getFilterIndex() ? $column->getFilterIndex() : $column->getIndex();
      if($columnIndex == 'pages_with_number_of_views.order_count')
      {
        $columnIndex = 'order_count';
        $collection->getSelect()->order($columnIndex.' '.strtoupper($column->getDir()));
      }
      if($columnIndex == 'total_sales'){
        $collection->getSelect()->order($columnIndex.' '.strtoupper($column->getDir()));
      }
      else{
        $collection->setOrder($columnIndex, strtoupper($column->getDir()));
      }
    }
    return $this;
  }

  protected function _prepareColumns()
  {
    $this->addExportType('*/*/exportCsv',
      Mage::helper('numberofview')->__('CSV'));

    $this->addColumn('entity_id', array(
      'header' => Mage::helper('customer')->__('ID'),
      'width' => '50px',
      'index' => 'entity_id',
      'type' => 'number',
    ));

    $this->addColumn('name', array(
      'header' => Mage::helper('customer')->__('Name'),
      'index' => 'name'
    ));
    $this->addColumn('email', array(
      'header' => Mage::helper('customer')->__('Email'),
      'width' => '150',
      'index' => 'email'
    ));

    $this->addColumn('order_count', array(
      'header' => Mage::helper('customer')->__('Total Orders'),
      'index' => 'order_count',
      'filter_index' => 'pages_with_number_of_views.order_count',
      'filter_condition_callback' => array($this, '_roleFilter'),
      'sort_condition_callback' => array($this, 'testtest'),
    ));

    $this->addColumn('total_sales', array(
      'header' => Mage::helper('sales')->__('Total Sales'),
      'index' => 'total_sales',
      'type' => 'price',
      'currency_code' => Mage::app()->getStore()->getBaseCurrency()->getCode(),
      'filter_condition_callback' => array($this, '_roleFilterTotalSales'),
    ));

    $groups = Mage::getResourceModel('customer/group_collection')
      ->addFieldToFilter('customer_group_id', array('gt' => 0))
      ->load()
      ->toOptionHash();

    $this->addColumn('group', array(
      'header' => Mage::helper('customer')->__('Group'),
      'width' => '100',
      'index' => 'group_id',
      'type' => 'options',
      'options' => $groups,
    ));

    $this->addColumn('customer_since', array(
      'header' => Mage::helper('customer')->__('Customer Since'),
      'type' => 'datetime',
      'align' => 'center',
      'index' => 'created_at',
      'gmtoffset' => true
    ));

    $this->addColumn('action', array(
      'header' => Mage::helper('numberofview')->__('Action'),
      'index' => 'entity_id',
      'filter' => false,
      'sortable' => false,
      'renderer' => 'Magebuzz_Numberofview_Block_Adminhtml_Sales_Orderbycustomerid',
    ));
    return parent::_prepareColumns();
  }

  protected function _roleFilter($collection, $column) {
    $collection = $this->getCollection();
    $value = $column->getFilter()->getValue();
    if (!$value) {
      return;
    }
    $collection->addSalesInfoPerCustomerFilterSalseOrder($value);
  }

  protected function _roleFilterTotalSales($collection, $column) {
    $collection = $this->getCollection();
    $value = $column->getFilter()->getValue();
    if (!$value) {
      return;
    }
    $collection->addSalesInfoPerCustomerFilterTotalSales($value);
  }
}
