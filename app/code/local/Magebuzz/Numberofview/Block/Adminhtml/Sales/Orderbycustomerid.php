<?php
class Magebuzz_Numberofview_Block_Adminhtml_Sales_Orderbycustomerid extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
    public function render(Varien_Object $row) {
      $customer_id = $row->getData($this->getColumn()->getIndex());
      $url = $this->getUrl("adminhtml/customer/edit/", array("id"=>$customer_id, 'active_tab' => 'orders'));
      return '<a href="'.$url.'">View</a>';
    }
}


