<?php
class Magebuzz_Numberofview_Block_Adminhtml_Customer_Edit_Tab_Stat extends Mage_Adminhtml_Block_Widget_Grid{

  public function __construct()
  {
    parent::__construct();
    $this->setId('numberofviewGrid');
    $this->setUseAjax(true);
    $this->setSaveParametersInSession(true);
  }
  protected function _prepareCollection()
  {
    $customerId = Mage::app()->getRequest()->getParam('id');

    $collection = Mage::getModel('numberofview/customer')->getCollection()->addFieldToFilter('customer_id', $customerId);
    $collection->getSelect()->joinLeft(Mage::getConfig()->getTablePrefix().
      'pages_with_number_of_views', 'main_table.url ='.Mage::getConfig()->getTablePrefix().
      'pages_with_number_of_views.url',array('page_detail'));
    $collection->addFieldToFilter('main_table.page_type','product');
    $this->setCollection($collection);
    return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
    $this->addExportType('*/*/exportCsv',Mage::helper('numberofview')->__('CSV'));

    $this->addColumn('page_detail', array(
      'header'    => Mage::helper('numberofview')->__('Product Name'),
      'align'     =>'left',
      'index'     => 'page_detail',
      'type'      => 'text'
    ));

    $this->addColumn('time', array(
      'header' => Mage::helper('customer')->__('Time'),
      'type' => 'datetime',
      'align' => 'center',
      'index' => 'time',
      'gmtoffset' => true
    ));

    $this->addColumn('page_view', array(
      'header'    => Mage::helper('numberofview')->__('Page View'),
      'align'     =>'left',
      'index'     => 'page_view',
      'type'      => 'text',
      'filterable'=> false,
    ));
    return parent::_prepareColumns();
  }
}
