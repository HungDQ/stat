<?php
class Magebuzz_Numberofview_Block_Adminhtml_Sales extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_sales';
        $this->_blockGroup = 'numberofview';
        $this->_headerText = Mage::helper('numberofview')->__('Report Order Stat');
        parent::__construct();
        $this->_removeButton('add');
    }
}
