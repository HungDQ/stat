<?php
class Magebuzz_Numberofview_Block_Adminhtml_Pageview_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
      parent::__construct();
      $this->setId('adminhtml_pageview');
      $this->setDefaultSort('id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(TRUE);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('numberofview/numberofview')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
      $this->addColumn('page_type', array(
          'header'    => Mage::helper('numberofview')->__('Page Type'),
          'align'     =>'left',
          'index'     => 'page_type',
          'type'      => 'text'
      ));

      $this->addColumn('page_detail', array(
          'header'    => Mage::helper('numberofview')->__('Page Detail'),
          'align'     => 'left',
          'index'     => 'page_detail',
          'type'      => 'text'
      ));

      $this->addColumn('url', array(
          'header'    => Mage::helper('numberofview')->__('URL'),
          'align'     => 'left',
          'index'     => 'url',
          'type'      => 'text'
      ));

      $this->addColumn('time', array(
        'header' => Mage::helper('customer')->__('Time'),
        'type' => 'datetime',
        'align' => 'center',
        'index' => 'time',
        'gmtoffset' => true
      ));

      $this->addColumn('page_view', array(
          'header'    => Mage::helper('numberofview')->__('No of views'),
          'align'     => 'left',
          'index'     => 'page_view',
      ));

      $this->addExportType('*/*/exportCsv',Mage::helper('numberofview')->__('CSV'));
      return parent::_prepareColumns();
    }


}

