<?php
class Magebuzz_Numberofview_Block_Numberofview extends Mage_Core_Block_Template {
    public function __construct() {
        parent::__construct();
        $collection = new stdClass();
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customer_id = 0;
        $condition = '';
        if (isset($customer)) {
            $customer_id = $customer['entity_id'];
            $condition = 'customer_id = "'.$customer_id.'"';
        }
        if(Isset($_GET['total_sales'])){
            if ($_GET['total_sales'] != "" ) {
                $collection = Mage::getModel('sales/order')->getCollection();
                $collection->getSelect()
                        ->columns('COUNT(customer_id) AS total_order')
                        ->columns('SUM(grand_total) AS total_sales')
                        ->where($condition)
                        ->group('customer_id');
            }
        }
        else if(Isset($_GET['page_stats'])){
            if ($_GET['page_stats'] != "" ) {
                $collection = Mage::getModel('numberofview/numberofview')->getCollection();

                if (isset($_POST["filter_page"])) {


                    if (isset($_POST["from_date_input"])) {
                        if ($_POST["from_date_input"] != "") {
                            $condition .= ' && time >= "'.$_POST["from_date_input"].'"';
                        }
                    }
                    if (isset($_POST["to_date_input"])) {
                        if ($_POST["to_date_input"] != "") {
                            $condition .= ' && time <= "'.$_POST["to_date_input"].'"';
                        }
                    }
                    if (isset($_POST["page_type"])) {
                        if ($_POST["page_type"] != "") {
                            $condition .= ' && page_type like "%'.$_POST["page_type"].'%"';
                        }
                    }
                    if (isset($_POST["page_detail"])) {
                        if ($_POST["page_detail"] != "") {
                            $condition .= ' && page_detail like "%'.$_POST["page_detail"].'%"';
                        }
                    }
                    if (isset($_POST["url"])) {
                        if ($_POST["url"] != "") {
                            $condition .= ' && url like "%'.$_POST["url"].'%"';
                        }
                    }
                    // k su dung dc
                    if (isset($_POST["number_view"])) {
                        if ($_POST["number_view"] != "") {
                            $having = 'view_numer like "%'.$_POST["number_view"].'%"';

                        }
                    }


                }
                if (isset($_POST["reset_filter_page"])){
                    if (isset($_POST["from_date_input"])) {
                        if ($_POST["from_date_input"] != "") {
                            $_POST["from_date_input"] = '';
                        }
                    }
                    if (isset($_POST["to_date_input"])) {
                        if ($_POST["to_date_input"] != "") {
                            $_POST["to_date_input"] = '';
                        }
                    }
                    if (isset($_POST["page_type"])) {
                        if ($_POST["page_type"] != "") {
                            $_POST["page_type"] = '';
                        }
                    }
                    if (isset($_POST["page_detail"])) {
                        if ($_POST["page_detail"] != "") {
                            $_POST["page_detail"] = '';
                        }
                    }
                    if (isset($_POST["url"])) {
                        if ($_POST["url"] != "") {
                            $_POST["url"] = '';
                        }
                    }
                    // k su dung dc
                    if (isset($_POST["number_view"])) {
                        if ($_POST["number_view"] != "") {
                            $having = '';

                        }
                    }

                }
                $collection->getSelect()
                        ->columns('COUNT(url) AS view_numer')
                        ->where($condition)
                        ->group('url');
                // click button generate file csv
                if (isset($_POST['csv_file_filter_page'])) {
                    if (isset($_POST["from_date_input"])) {
                        if ($_POST["from_date_input"] != "") {
                            $condition .= ' && time >= "'.$_POST["from_date_input"].'"';
                        }
                    }
                    if (isset($_POST["to_date_input"])) {
                        if ($_POST["to_date_input"] != "") {
                            $condition .= ' && time <= "'.$_POST["to_date_input"].'"';
                        }
                    }
                    if (isset($_POST["page_type"])) {
                        if ($_POST["page_type"] != "") {
                            $condition .= ' && page_type like "%'.$_POST["page_type"].'%"';
                        }
                    }
                    if (isset($_POST["page_detail"])) {
                        if ($_POST["page_detail"] != "") {
                            $condition .= ' && page_detail like "%'.$_POST["page_detail"].'%"';
                        }
                    }
                    if (isset($_POST["url"])) {
                        if ($_POST["url"] != "") {
                            $condition .= ' && url like "%'.$_POST["url"].'%"';
                        }
                    }
                    // k su dung dc
                    if (isset($_POST["number_view"])) {
                        if ($_POST["number_view"] != "") {
                            $having = 'view_numer like "%'.$_POST["number_view"].'%"';

                        }
                    }
                    $collection->getSelect()
                        ->columns('COUNT(url) AS view_numer')
                        ->where($condition)
                        ->group('url');
                    $data = "Page type, Page Detail, URL, No of view \n";
                    foreach ($collection->getData() as $value) {
                        $data .= $value['page_type'].",".$value['page_detail'].",".$value['url'].",".$value['view_numer']."\n";
                      }
                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                    header("Content-Length: " . strlen($data));
                    // Output to browser with appropriate mime type, you choose ;)
                    header("Content-type: text/x-csv");
                    //header("Content-type: text/csv");
                    //header("Content-type: application/csv");
                    header("Content-Disposition: attachment; filename=PageStats.csv");
                    echo $data;
                    exit;
                }

                // Searching flow number-view page
                // $collection->getSelect()
                //         ->columns('COUNT(url) AS view_numer')
                //         ->where($condition)
                //         ->group('url')
                //         ->having($having);
            }
        }
        else{
             Mage::app()->getResponse()->setRedirect('/no-route.html');
        }
        $this->setCollection($collection);
    }
    //prepare layout
    public function _prepareLayout() {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'numberofview.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,50=>50,100=>100,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }
    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }
}
