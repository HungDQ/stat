jQuery(document).ready(function(){

  jQuery('#numberofviewGrid table.actions .export.a-right button.scalable.task').click(function(){
    urlExportCSV();
  });


  jQuery( "#numberofviewGrid table.actions td.filter-actions button[title='Reset Filter']" ).click(function(){
    var currenturl      = window.location.href;
    var url_replace = currenturl.replace('filter//form_key/zrVhl009KOZXnkwb/','');
    var url = url_replace.split("?",1);
    window.location = url + 'filter//form_key/zrVhl009KOZXnkwb/';
  });

  // remove button
  jQuery('td.form-buttons button.add').remove();
  jQuery('div#customer_info_tabs_new_tab_content .content-header').remove();
  jQuery('div.action input.scalable').click(function(){
      var value_from = jQuery('.my-picker.from-date').val();s
      var value_to = jQuery('.my-picker.to-date').val();
      // alert(value_from);
      if (value_from != "") {

          if (!validateDate(value_from)) {
              alert('Invalid date format');
              return false;
          }
      }
      if (value_to != "") {
          if (!validateDate(value_to)) {
              alert('Invalid date format');
              return false;
          }
      }
  });

  // Show date-picker
  jQuery( ".my-picker" ).datepicker({
    inline: true,
    isRTL: true,
    showOtherMonths: true,
    dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    dateFormat: "yy-mm-dd"
  });


  function urlExportCSV () {
    var currenturl      = window.location.href;
    var url = currenturl.split("?",1);
    var param = currenturl.replace(url,'');
    var currentparam = currenturl.split("&customer_name",1);
    customer_name = '';
    customer_email = '';
    page_type = '';
    page_detail = '';
    url_ = '';
    customer_company = '';
    customer_firstname = '';
    customer_lastname = '';
    customer_groupname = '';
    addparam = '';

    if (jQuery('input#numberofviewGrid_filter_customer_name').length > 0){
       customer_name = jQuery('input#numberofviewGrid_filter_customer_name').val();
    }
    if (jQuery('input#numberofviewGrid_filter_customer_email').length > 0){
       customer_email = jQuery('input#numberofviewGrid_filter_customer_email').val();
    }
    if (jQuery('input#numberofviewGrid_filter_page_type').length > 0){
       page_type = jQuery('input#numberofviewGrid_filter_page_type').val();
    }
    if (jQuery('input#numberofviewGrid_filter_page_detail').length > 0){
       page_detail = jQuery('input#numberofviewGrid_filter_page_detail').val();
    }
    if (jQuery('input#numberofviewGrid_filter_url').length > 0){
       url_ = jQuery('input#numberofviewGrid_filter_url').val();
    }
    if (jQuery('input#numberofviewGrid_filter_customer_company').length > 0){
       customer_company = jQuery('input#numberofviewGrid_filter_customer_company').val();
    }
    if (jQuery('input#numberofviewGrid_filter_customer_firstname').length > 0){
       customer_firstname = jQuery('input#numberofviewGrid_filter_customer_firstname').val();
    }
    if (jQuery('input#numberofviewGrid_filter_customer_lastname').length > 0){
       customer_lastname = jQuery('input#numberofviewGrid_filter_customer_lastname').val();
    }
    if (jQuery('input#numberofviewGrid_filter_customer_groupname').length > 0){
       customer_groupname = jQuery('input#numberofviewGrid_filter_customer_groupname').val();
    }


    if (param.length == 0 ) {

      if (jQuery('body.numberofview-adminhtml-order-index').length > 0) {
        // page order
        addparam = '?customer_name=' + customer_name + '&customer_email=' + customer_email;
      }
      if (jQuery('body.numberofview-adminhtml-index-index').length > 0) {
        // page Page view
        addparam = '?page_type=' + page_type + '&page_detail=' + page_detail + '&url_=' + url_;

      }
      if (jQuery('body.numberofview-adminhtml-userinfo-index').length > 0) {
        addparam = '?customer_email=' + customer_email + '&customer_company=' +customer_company + '&customer_firstname=' +customer_firstname + '&customer_lastname=' +customer_lastname;
      }
    }else{
      if (customer_name != '') {
        param = param.split("&customer_name",1);
        param = param + '&customer_name='+customer_name;
      }
      if (customer_email != '') {
        param = param.split("&customer_email",1);
        param = param + '&customer_email='+customer_email;
      }
      // page view
      if (page_type != '') {
        param = param.split("&page_type",1);
        param = param + '&page_type='+page_type;
      }
      if (page_detail != '') {
        param = param.split("&page_detail",1);
        param = param + '&page_detail='+page_detail;
      }
      if (url_ != '') {
        param = param.split("&url_",1);
        param = param + '&url_='+customer_email;
      }
      if (customer_company != '') {
        param = param.split("&customer_company",1);
        param = param + '&customer_company='+customer_company;
      }
      if (customer_firstname != '') {
        param = param.split("&customer_firstname",1);
        param = param + '&customer_firstname='+customer_firstname;
      }
      if (customer_lastname != '') {
        param = param.split("&customer_lastname",1);
        param = param + '&url_='+customer_lastname;
      }
      if (customer_groupname != '') {
        param = param.split("&customer_groupname",1);
        param = param + '&customer_groupname='+customer_groupname;
      }

    }
    window.location = url+param+addparam+'&export=1';
  }

  // validation field date
  function validateDate(date) {
      return (/^\d{4}-\d{2}-\d{2}$/).test(date);
  }
});
